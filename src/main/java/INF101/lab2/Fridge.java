package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    public ArrayList<FridgeItem> items = new ArrayList<FridgeItem>();
    public int maxSize = 20;
    public ArrayList<FridgeItem> expired = new ArrayList<FridgeItem>();

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public void emptyFridge() {
        items.clear();
        
    }

    @Override
    public int totalSize() {
        return maxSize;
    }
    
    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if (nItemsInFridge()!=0){
            items.remove(item);
        } else {
            throw new NoSuchElementException("Fridge is empty");
        }
        
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if (nItemsInFridge()<maxSize){
        items.add(item);
        return true;
    } else{
        return false;
    }}

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        for (FridgeItem item : items){
            if (item.hasExpired()){
                expired.add(item);
            }
        }
        for (FridgeItem badFood : expired){
            takeOut(badFood);
        }
        return expired;
    }
}
